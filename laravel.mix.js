var _a;
let laravelMix = require('laravel-mix');
let glob = require('glob');
let path = require('path');
// unfortunately it watches for already created files only and ignores any new,
// the watcher should be restarted for new
// https://github.com/laravel-mix/laravel-mix/issues/3003
class FrontBlockAssets {
    constructor(config) {
        this.scssFiles = [];
        this.tsFiles = [];
        this.config = config;
        this.absolutePathToBlocks = path.resolve(process.cwd() + '/' + config.frontBlocks.relativePathToBlocks);
    }
    static getInstance() {
        let isWrong = false;
        try {
            let config = require(process.cwd() + '/front-block-assets.json');
            if (!config.hasOwnProperty('frontBlocks') ||
                !config.frontBlocks.hasOwnProperty('relativePathToBlocks') ||
                !config.frontBlocks.hasOwnProperty('relativePathToNodeModules')) {
                isWrong = true;
                throw new Error('wrong');
            }
            return new FrontBlockAssets(config);
        }
        catch (exception) {
            let error = isWrong ?
                'front-block-assets.json file is wrong' :
                'front-block-assets.json file is missing';
            throw new Error(error);
        }
    }
    setupLaravelMix() {
        laravelMix.setPublicPath(this.config.frontBlocks.relativePathToBlocks);
        laravelMix.webpackConfig({
            resolve: {
                modules: [
                    process.cwd() + '/' + this.config.frontBlocks.relativePathToNodeModules,
                ],
            },
            // increase speed
            watchOptions: {
                ignored: /node_modules/,
            },
        });
        // makes babel is dependent on package.json browserslist, so will skip additional polyfills
        laravelMix.babelConfig({
            'presets': [
                [
                    '@babel/preset-env',
                    {},
                ],
            ],
        });
        if (this.config.hasOwnProperty('terser') &&
            this.config.terser.hasOwnProperty('keepClassnames') &&
            this.config.terser.keepClassnames) {
            laravelMix.options({
                terser: {
                    terserOptions: {
                        keep_classnames: new RegExp(this.config.terser.keepClassnames),
                        keep_fnames: new RegExp(this.config.terser.keepClassnames),
                    },
                },
            });
        }
    }
    readFiles() {
        this.scssFiles = glob.sync('**/*.scss', {
            cwd: this.absolutePathToBlocks,
        });
        this.tsFiles = glob.sync('**/*.ts', {
            cwd: this.absolutePathToBlocks,
        });
        // make file paths absolute
        this.scssFiles.forEach((file, index, array) => {
            array[index] = this.absolutePathToBlocks + '/' + file;
        });
        this.tsFiles.forEach((file, index, array) => {
            array[index] = this.absolutePathToBlocks + '/' + file;
        });
    }
    mixScssFiles() {
        let browsers = [];
        if (this.config.hasOwnProperty('autoprefixer') &&
            this.config.autoprefixer.hasOwnProperty('browsers')) {
            browsers = this.config.autoprefixer.browsers;
        }
        this.scssFiles.forEach((absPathToScssFile, index) => {
            let absPathToMinCssFile = absPathToScssFile.replace('.scss', '.min.css');
            laravelMix.sass(absPathToScssFile, absPathToMinCssFile, {
                sassOptions: {
                    // otherwise "@use 'package'" won't work
                    includePaths: [this.config.frontBlocks.relativePathToNodeModules],
                },
            }).options({
                autoprefixer: {
                    options: {
                        browsers: browsers,
                    },
                },
                // increase speed
                processCssUrls: false,
            });
        });
    }
    mixTypescriptFiles() {
        this.tsFiles = this.tsFiles.filter((pathToFile) => {
            return !pathToFile.match('stub\.ts');
        });
        this.tsFiles.forEach((absPathToJsFile, index) => {
            let absPathToMinJsFile = absPathToJsFile.replace('.ts', '.min.js');
            laravelMix.ts(absPathToJsFile, absPathToMinJsFile);
        });
    }
    mixFiles() {
        // it could be written like : laravelMix.ts(this.blocksAbsPath + '/**/*.ts',this.blocksAbsPath)
        // but in this case would have only 1 output file
        // will contain a list of dependencies
        laravelMix.js(this.absolutePathToBlocks + '/stub.ts', this.absolutePathToBlocks + '/stub.min.js');
        this.mixScssFiles();
        this.mixTypescriptFiles();
    }
    setup() {
        this.setupLaravelMix();
        this.readFiles();
        this.mixFiles();
    }
}
(_a = FrontBlockAssets.getInstance()) === null || _a === void 0 ? void 0 : _a.setup();
