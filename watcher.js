let { FSWatcher, watch } = require("chokidar");
let { spawn, exec, ChildProcess } = require("child_process");
let pathModule = require('path');
const processModule = require('node:process');
class Watcher {
    constructor(config) {
        this.config = config;
        this.absolutePathToBlocks = pathModule.resolve(processModule.cwd() + '/' + config.frontBlocks.relativePathToBlocks);
    }
    log(state, info = '') {
        let date = new Date();
        console.log(date.getHours() + ':' + date.getMinutes() + ' ' + state + ' ' + info);
    }
    static getConfig() {
        let isWrong = false;
        try {
            let config = require(processModule.cwd() + '/front-block-assets.json');
            if (!config.hasOwnProperty('frontBlocks') ||
                !config.frontBlocks.hasOwnProperty('relativePathToBlocks')) {
                isWrong = true;
                throw new Error('wrong');
            }
            if (config.hasOwnProperty('sshWatcher')) {
                if (!config.sshWatcher.hasOwnProperty('remoteHost') ||
                    !config.sshWatcher.hasOwnProperty('remotePathToBlocks') ||
                    !config.sshWatcher.hasOwnProperty('localPathToMasterConnection')) {
                    isWrong = true;
                    throw new Error('wrong');
                }
            }
            return config;
        }
        catch (exception) {
            let error = isWrong ?
                'front-block-assets.json file is wrong' :
                'front-block-assets.json file is missing';
            throw new Error(error);
        }
    }
}
class LaravelMixWatcher extends Watcher {
    constructor(config) {
        super(config);
        this.config = config;
        this.mixProcess = null;
        this.watcher = null;
    }
    mix(reasonPath) {
        if (this.mixProcess) {
            this.mixProcess.stdin.pause();
            this.mixProcess.kill();
            this.log('restarting', pathModule.basename(reasonPath));
        }
        this.mixProcess = spawn("yarn", ["watch-dev"]);
        this.log('watching');
        // display all output
        /*this.mixProcess.stdout.on('data', data => {
            console.log(data.toString())
        })*/
    }
    watch() {
        processModule.on('SIGINT', (code) => {
            if (this.mixProcess) {
                this.mixProcess.stdin.pause();
                this.mixProcess.kill();
            }
            this.watcher.close();
        });
        let excludeFiles = null;
        if (this.config.hasOwnProperty('mixWatcher') &&
            this.config.mixWatcher.hasOwnProperty('excludeFiles')) {
            excludeFiles = new RegExp(this.config.mixWatcher.excludeFiles);
        }
        this.watcher = watch(this.absolutePathToBlocks, {
            ignored: excludeFiles,
            ignoreInitial: true,
        }).on('add', this.mix.bind(this))
            .on('unlink', this.mix.bind(this))
            .on('ready', this.mix.bind(this));
    }
}
class SshWatcher extends Watcher {
    constructor(config) {
        super(config);
        this.watcher = null;
    }
    sshCopyFile(file) {
        let relativePath = file.replace(this.absolutePathToBlocks, '');
        let localFile = this.absolutePathToBlocks + relativePath;
        let remoteFile = this.config.sshWatcher.remotePathToBlocks + relativePath;
        let remoteDir = pathModule.dirname(remoteFile);
        let uploadFile = () => {
            exec("scp -o 'ControlPath=" + this.config.sshWatcher.localPathToMasterConnection + "' " + localFile + ' ' +
                this.config.sshWatcher.remoteHost + ':' + remoteFile, () => {
                this.log('uploaded', relativePath);
            });
        };
        // check if dir exists
        exec("ssh " + this.config.sshWatcher.remoteHost + " -S " + this.config.sshWatcher.localPathToMasterConnection + " 'test -d " + remoteDir + " && echo exists'", (error, output) => {
            if ('exists' === output.trim()) {
                uploadFile();
                return;
            }
            this.log('dir is missing, creating', relativePath);
            exec("ssh " + this.config.sshWatcher.remoteHost + " -S " + this.config.sshWatcher.localPathToMasterConnection + " 'mkdir -p " + remoteDir + "'", () => {
                uploadFile();
            });
        });
    }
    sshRemoveFile(file) {
        let relativePath = file.replace(this.absolutePathToBlocks, '');
        let remoteFile = this.config.sshWatcher.remotePathToBlocks + relativePath;
        exec("ssh " + this.config.sshWatcher.remoteHost + " -S " + this.config.sshWatcher.localPathToMasterConnection + " 'rm " + remoteFile + "'", () => {
            this.log('removed', relativePath);
        });
    }
    createMasterConnection() {
        let command = spawn("ssh", ["-nNf", "-M", "-S", this.config.sshWatcher.localPathToMasterConnection, this.config.sshWatcher.remoteHost]);
        command.on('exit', () => {
            this.log('sshMasterOpen');
        });
    }
    closeMasterConnection() {
        exec("ssh -O exit -o ControlPath=" + this.config.sshWatcher.localPathToMasterConnection + " " + this.config.sshWatcher.remoteHost, () => {
            this.log('sshMasterClosed');
            processModule.exit();
        });
    }
    watch() {
        this.createMasterConnection();
        processModule.on('SIGINT', (code) => {
            this.watcher.close();
            this.closeMasterConnection();
        });
        let excludeFiles = null;
        if (this.config.hasOwnProperty('sshWatcher') &&
            this.config.sshWatcher.hasOwnProperty('excludeFiles')) {
            excludeFiles = new RegExp(this.config.sshWatcher.excludeFiles);
        }
        this.watcher = watch(this.absolutePathToBlocks, {
            ignored: excludeFiles,
            ignoreInitial: true,
        }).on('change', this.sshCopyFile.bind(this))
            .on('add', this.sshCopyFile.bind(this))
            .on('unlink', this.sshRemoveFile.bind(this));
    }
}
let config = Watcher.getConfig();
let mixWatcher = new LaravelMixWatcher(config);
let sshWatcher = new SshWatcher(config);
mixWatcher.watch();
if (config.hasOwnProperty('sshWatcher')) {
    sshWatcher.watch();
}
