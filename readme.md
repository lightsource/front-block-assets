# Front Block Assets

## What is it?

Assets manager for the [FrontBlocks](https://gitlab.com/lightsource/front-blocks) package that allows build
typescript (`.ts`) and sass (`.scss`) files
files without a hassle. It's a wrapper above [LaravelMix](https://laravel-mix.com/).

The key feature that it scans the blocks folder and creates a separate task for each file, so each file has its own
independent output.

## How to use?

1. Install the package

`yarn add @lightsourcee/front-block-assets`

2. Create the config - `front-block-assets.json`

```json
{
  "frontBlocks": {
    "relativePathToBlocks": "../src/Blocks",
    "relativePathToNodeModules": "./node_modules"
  },
  "terser": {
    "keepClassnames": ".*Element"
  },
  "autoprefixer": {
    "browsers": [
      "last 3 Firefox versions",
      "last 3 Chrome versions",
      "last 3 Safari versions",
      "last 3 FirefoxAndroid versions",
      "last 3 ChromeAndroid versions",
      "last 3 iOS versions"
    ]
  }
}
```

In this example we have a special setting for `terser`, to keep classes of [Catalyst](https://catalyst.rocks/) elements
as is.

3. Add yarn commands to your package.json

(Note: we've also globally defined the "browserslist" option, otherwise webpack output will create too many polyfills, which is critical for us, as each file is a separate build)

```json
{
  "scripts": {
    "build-prod": "npx mix -p --mix-config=node_modules/@lightsourcee/front-block-assets/laravel.mix.js",
    "build-dev": "npx mix --mix-config=node_modules/@lightsourcee/front-block-assets/laravel.mix.js",
    "watch-prod": "cross-env NODE_ENV=production npx mix watch --mix-config=node_modules/@lightsourcee/front-block-assets/laravel.mix.js",
    "watch-dev": "npx mix watch --mix-config=node_modules/@lightsourcee/front-block-assets/laravel.mix.js"
  },
  "browserslist": [
    "last 3 Firefox versions",
    "last 3 Chrome versions",
    "last 3 Safari versions",
    "last 3 FirefoxAndroid versions",
    "last 3 ChromeAndroid versions",
    "last 3 iOS versions"
  ]
}
```

4. Add two files to the blocks dir: `stub.ts` and `tsconfig.json`  
The first is empty, webpack will write here a list of files and other trash stuff. The second is for typescript. Can be like below:

```json
{
  "compilerOptions": {
    "moduleResolution": "node",
    "target": "es6",
    "lib": [
      "dom",
      "es2020"
    ],
    "experimentalDecorators": true,
    "noImplicitAny": false,
    "sourceMap": false,
    "baseUrl": "../node_modules"
  }
}
```

5. Build

Now you can run `yarn build-prod` and others. Enjoy

## Advanced usage

### 1. Mix watcher

Unfortunately, LaravelMix watches only for existing at the start moment files and ignores any new
files ([read more](https://github.com/laravel-mix/laravel-mix/issues/3003)).

So it's possible to solve the issue only by watching the folder and restarting Mix when new files appear on our side.

It's done in this package using the amazing [Chokidar](https://www.npmjs.com/package/chokidar) watcher.

To use this option:

a) add the `mixWatcher` argument to the `front-block-assets.json` file

```json
{
  "mixWatcher": {
    "excludeFiles": "(\\.twig)|(\\.php)|(\\.min\\.css)|(\\.min\\.js)|(mix-manifest.json)"
  }
}
```

b) run `node ./node_modules/@lightsourcee/front-block-assets/watcher.js` from the right folder.

That's it. It'll automatically run `yarn watch-dev` command, and will restart every time a new file appears.
Press `Ctrl+C` to stop the watcher process (it'll stop LaravelMix too).

We can't use the `scripts` section like we did before, as it'll create extra process, and you'll need to
press `Ctrl+C` (`exit` single) two times.

You can create an alias to shorter the command, e.g.

watcher.js

```js
require('@lightsourcee/front-block-assets/watcher')
```

And then just execute `node watcher.js`

### 2. SCP uploader

Many IDEs have auto-uploading by FTP. But as we build assets using LaravelMix, i.e. outside of IDE, some of them can
not notice changes. Like PHPStorm, that does file sync only after switching to another window, which is annoying, as you
need to switch to another window and back to get uploading done.

For users on Linux, workaround can be using the `scp` package that allows to sync/upload local files and folders to a
remote host.

This package has the ssh watcher feature, if you're on Linux you can disable uploading `.scss` and `.ts` files in your
IDE (add to
upload ignores list) and use the feature.

a) Add info about your remove host to the `~/.ssh/config`

(Update the values to yours)

```
Host admin.wordpress.org
 HostName 1.1.1.1
 User admin
 Port 11111
```

b) Make sure the `scp` package is installed

`man scp`

c) Add the `sshWatcher` section to the `front-block-assets.json`

(Update the values to yours)

```json
{
  "sshWatcher": {
    "remoteHost": "admin.wordpress.org",
    "remotePathToBlocks": "/home/admin/domains/wordpress.org/public_html/wp-content/themes/wp/src/Blocks",
    "localPathToMasterConnection": "/home/admin/.ssh/ctl/wordpress",
    "excludeFiles": "(\\.twig)|(\\.php)|(\\.scss)|(\\.ts)|(mix-manifest.json)|(stub\\.min\\.js)"
  }
}
```

The package will use the master connection feature to avoid time-wasting on connections. Nothing is required from your
side, just define the path to the socket
file that will be created automatically (`localPathToMasterConnection`). Name can be any.

FYI: SSH master connection

It allows to reuse the single connection between different uploads.
So this master connection will be open once (at the start) and will be using for uploading during the whole time. When
you're finishing the watcher process, the package will close the master connection and remove the file.

d) That's it. The ssh watcher lives in the same file as the Mix watcher (see above), so you don't need to make extra
changes.
As soon you've added the `sshWatcher` section to the config, it'll use the feature within
the `@lightsourcee/front-block-assets/watcher` file. Enjoy